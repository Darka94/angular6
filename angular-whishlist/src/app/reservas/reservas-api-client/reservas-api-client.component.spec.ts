import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservasApiClientComponent } from './reservas-api-client.component';

describe('ReservasApiClientComponent', () => {
  let component: ReservasApiClientComponent;
  let fixture: ComponentFixture<ReservasApiClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReservasApiClientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservasApiClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
